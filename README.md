Monster Rampart is my second game in Unity (and first one finished). The aim is to defend the castle from hordes of monsters. I created it after being asked by my 8 year old nephew to create game where you shoot zombiec. It is inspired by Rampart - old game from 1991. It's a mix of tower defense and Tetris.

Genre: Tower Defense

Features / solutions:

- 2D, Tower Defense / Shooter / Tetris, highscore system, saving system, localization (polish & anglish languafes to choose), quite high difficulty level, possibility to easily create new levels thanks to usage of Scriptable Objects
- platform - Windows

Link to YT video in which I present the game (video in polish):
https://www.youtube.com/watch?v=TM5eNuuFT44

To play the game pull the repository, add project to Unity and build it. It should work (It works for me :)

Unity version: 2020.1.0f1

