﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreenMonster : MonoBehaviour
{
    private SplashScreenController controller;

    private void Awake() => controller = FindObjectOfType<SplashScreenController>();


    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(collision.gameObject);
        controller.StartExplosion();
    }
}
