﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class SplashScreenController : MonoBehaviour
{
    [SerializeField] private GameObject canon;
    [SerializeField] private GameObject castle;
    [SerializeField] private GameObject monster;
    [SerializeField] private GameObject canonDestination;
    [SerializeField] private GameObject canonBallStart;
    [SerializeField] private GameObject monsterDestination;
    [SerializeField] private GameObject canonBallPrefab;
    [SerializeField] private VideoPlayer player;
    [SerializeField] private GameObject sceneObjects;
    [SerializeField] private GameObject title;

    IEnumerator Start()
    {
        yield return StartCoroutine(CanonEnter());
        StartCoroutine(MonsterEnter());
        yield return new WaitForSeconds(2f);
        canon.GetComponent<Animator>().SetBool("firing", true);
        yield return new WaitForSeconds(1f);
        GameObject canonBall = Instantiate(canonBallPrefab, canonBallStart.transform.position, Quaternion.identity);
        canon.transform.parent = canon.transform;
        canonBall.GetComponent<Rigidbody2D>().velocity = transform.right * 10f;
    }

    public void StartExplosion()
    {
        sceneObjects.SetActive(false);
        player.Play();
        StartCoroutine(WaitForExplosionEnd());
    }

    private IEnumerator CanonEnter()
    {
        yield return new WaitForSeconds(1.5f);
        canon.GetComponent<Animator>().SetBool("moving", true);

        while (Vector3.Distance(canon.transform.position, canonDestination.transform.position) > 0.01f)
        {
            canon.transform.position = Vector3.MoveTowards(canon.transform.position, canonDestination.transform.position, 3f * Time.deltaTime);
            yield return null;
        }
        canon.GetComponent<Animator>().SetBool("moving", false);
    }

    private IEnumerator MonsterEnter()
    {
        while (Vector3.Distance(monster.transform.position, monsterDestination.transform.position) > 0.01f)
        {
            monster.transform.position = Vector3.MoveTowards(monster.transform.position, monsterDestination.transform.position, 3f * Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator WaitForExplosionEnd()
    {
        yield return new WaitForSeconds(1f);
        title.SetActive(true);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
