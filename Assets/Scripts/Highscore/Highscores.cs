﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Highscores
{
    private const int HIGHSCORE_LIST_SIZE = 10;
    private List<int> highscoreList;

    public Highscores()
    {
        highscoreList = new List<int>();
    }

    public bool IsNewHighscore(int newHighscore)
    {
        if (highscoreList.Count < HIGHSCORE_LIST_SIZE)
        {
            return true;
        }
        else
        {
            int minValue = highscoreList.Min();
            if (minValue >= newHighscore)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public void AddNewHighscore(int newHighscore)
    {
        if (highscoreList.Count < HIGHSCORE_LIST_SIZE)
        {
            highscoreList.Add(newHighscore);
        }
        else
        {
            int minValue = highscoreList.Min();
            int minValueIndex = highscoreList.IndexOf(minValue);
            highscoreList[minValueIndex] = newHighscore;
        }
    }

    public List<int> HighscoreList
    {
        get
        {
            return highscoreList;
        }
        set
        {
            highscoreList = value;
        }
    }
}
