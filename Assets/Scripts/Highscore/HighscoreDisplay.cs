﻿using System.Collections;
using System.Collections.Generic;
using System.Transactions;
using UnityEngine;

public class HighscoreDisplay : MonoBehaviour
{
    [SerializeField] private GameObject noHighscoresText;
    [SerializeField] private HighscoreText highscoreText;
    [SerializeField] private GameObject highscoreList;

    private int[] highscores;

    public void DisplayHighscores()
    {
        gameObject.SetActive(true);
        highscores = HighscoreSystem.GetHighscoresSortedList();

        if (highscores == null || highscores.Length == 0)
        {
            noHighscoresText.SetActive(true);
        } else
        {
            foreach (int highscore in highscores)
            {
                HighscoreText newHighscoreText = Instantiate(highscoreText);
                newHighscoreText.transform.parent = highscoreList.transform;
                newHighscoreText.SetHighscore(highscore);
            }
        }
    }

    public void HideHighScores()
    {
        foreach (Transform highscoreTextTransform in highscoreList.transform)
        {
            if (highscoreTextTransform.gameObject.GetComponent<HighscoreText>())
            {
                Destroy(highscoreTextTransform.gameObject);
            }
        }
        gameObject.SetActive(false);
        noHighscoresText.SetActive(false);
    }
}
