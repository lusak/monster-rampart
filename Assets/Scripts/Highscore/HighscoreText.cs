﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighscoreText : MonoBehaviour
{
    private TextMeshProUGUI text;

    public void SetHighscore(int highscore)
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = highscore.ToString();
        text.color = Random.ColorHSV(0f, 1f, 0f, 1f, 0f, 1f, 1f, 1f);
    }
}
