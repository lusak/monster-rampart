﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class ShapeInfo : ScriptableObject
{
    public Vector2[] positions;
}
