﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public const string SAVE_GAME_TEXT = "save_game_text";
    public const string OVERWRITE_GAME_TEXT = "overwrite_text";
    public const string GAME_SAVED_TEXT = "save_game_text_4";
    public const int startMenuSceneIndex = 1;

    public static PauseMenu instance;

    public static bool GamePaused = false;

    public GameObject pauseMenuUI;
    public GameObject optionsButtonGroup;
    public GameObject mainButtonGroup;
    public GameObject loadGameButtons;
    public GameObject saveGameButtons;
    public GameObject confirmSaveGameButtons;
    public GameObject quitToMenuButtons;
    public GameObject saveList;
    public GameObject loadAndDeleteButtons;
    public GameObject noSavesText;
    public GameObject savesScrollBar;
    public GameObject gameOverMenu;
    public SaveButton saveButton;

    //SaveMenu objects
    public LocalizedText saveGameText;
    public InputField inputField;
    public GameObject yesToOverwriteGame;
    public GameObject noToOverwriteGame;
    public GameObject saveMenuButton;

    private bool gameOver = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (this != instance)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (pauseMenuUI == null)
        {
            Debug.LogError("No pause menu selected");
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().buildIndex > 1 && !gameOver)
            {
                if (GamePaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
    }

    public void Resume()
    {
        mainButtonGroup.SetActive(true);
        optionsButtonGroup.SetActive(false);
        saveGameButtons.SetActive(false);
        confirmSaveGameButtons.SetActive(false);
        quitToMenuButtons.SetActive(false);
        pauseMenuUI.SetActive(false);
        loadGameButtons.SetActive(false);
        gameOverMenu.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;
    }
    public void SaveGame(bool overWrite)
    {
        string saveName = inputField.text;
        if (string.IsNullOrEmpty(saveName))
        {
            inputField.placeholder.color = Color.red;
            return;
        }

        if (SaveSystem.CheckSaveFileExists(saveName))
        {
            if (overWrite)
            {
                SaveManager.instance.SaveGame(saveName);
                saveGameText.UpdateText(GAME_SAVED_TEXT);
                yesToOverwriteGame.SetActive(false);
                noToOverwriteGame.SetActive(false);
                saveMenuButton.SetActive(true);
            }
            saveGameText.UpdateText(OVERWRITE_GAME_TEXT);
            yesToOverwriteGame.SetActive(true);
            noToOverwriteGame.SetActive(true);
            saveMenuButton.SetActive(false);
            return;
        }

        SaveManager.instance.SaveGame(saveName);
        saveGameText.UpdateText(GAME_SAVED_TEXT);
    }

    public void ResetSaveGameScreen()
    {
        saveGameText.UpdateText(SAVE_GAME_TEXT);
        inputField.text = "";
    }

    public void ShowLoadMenu()
    {
        foreach(Transform save in saveList.transform)
        {
            Destroy(save.gameObject);
        }

        string[] saveNames = SaveSystem.GetSavesPaths();

        if (saveNames.Length > 0)
        {
            for (int i = 0; i < saveNames.Length; i++)
            {
                SaveButton sButton = Instantiate(saveButton);
                sButton.transform.SetParent(saveList.transform);
                sButton.SetSaveButton(saveNames[i]);
            }
            loadAndDeleteButtons.SetActive(true);
            savesScrollBar.SetActive(true);
            noSavesText.SetActive(false);

        } else
        {
            loadAndDeleteButtons.SetActive(false);
            savesScrollBar.SetActive(false);
            noSavesText.SetActive(true);
        }

        mainButtonGroup.SetActive(false);
        loadGameButtons.SetActive(true);
    }

    public void ShowGameOverMenu()
    {
        gameOver = true;
        pauseMenuUI.SetActive(true);
        mainButtonGroup.SetActive(false);
        gameOverMenu.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;
    }

    public void HideQuitToMainMenuButtonGroup()
    {
        if (gameOver)
        {
            saveGameButtons.SetActive(false);
            quitToMenuButtons.SetActive(false);
            ResetSaveGameScreen();
            gameOverMenu.SetActive(true);
        } else
        {
            saveGameButtons.SetActive(false);
            quitToMenuButtons.SetActive(false);
            ResetSaveGameScreen();
            mainButtonGroup.SetActive(true);
        }
    }

    public void Retry()
    {
        Resume();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadMainMenu()
    {
        Resume();
        SceneManager.LoadScene(startMenuSceneIndex);
    }

    public void LoadGame()
    {
        Resume();
        SaveManager.instance.LoadGame();
    }

    public void DeleteSave()
    {
        SaveManager.instance.DeleteSave();
        ShowLoadMenu();
    }

    public void QuitGame() => Application.Quit();

    public bool GameOver
    {
        get
        {
            return gameOver;
        }

        set
        {
            gameOver = value;
        }
    }
}
