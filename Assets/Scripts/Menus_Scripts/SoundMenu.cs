﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Toggle muteToggle;
    public Slider slider;

    private void Start()
    {
        SetSlider();
        SetToggle();
    }

    public void SetVolume(float volume) => audioMixer.SetFloat("volume", volume);

    public void SetMenu()
    {
        SetSlider();
        SetToggle();
    }

    public void Mute()
    {
        if (muteToggle.isOn)
        {
            AudioListener.volume = 0f;
        }
        else
        {
            AudioListener.volume = 1f;
        }
    }

    private void SetToggle()
    {
        if (AudioListener.volume == 0f)
        {
            muteToggle.isOn = true;
        }
        else
        {
            muteToggle.isOn = false;
        }
    }

    private void SetSlider()
    {
        float value;
        bool result = audioMixer.GetFloat("volume", out value);
        if (result)
        {
            slider.value = value;
        }
    }
}
