﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public GameObject saveList;
    public SaveButton saveButton;
    public GameObject loadAndDeleteButtons;
    public GameObject noSavesText;
    public GameObject savesScrollBar;
    public GameObject loadGameMenu;

    public void ShowLoadMenu()
    {
        foreach (Transform save in saveList.transform)
        {
            Destroy(save.gameObject);
        }

        string[] saveNames = SaveSystem.GetSavesPaths();

        if (saveNames.Length > 0)
        {
            for (int i = 0; i < saveNames.Length; i++)
            {
                SaveButton sButton = Instantiate(saveButton);
                sButton.transform.SetParent(saveList.transform);
                sButton.SetSaveButton(saveNames[i]);
            }
            loadAndDeleteButtons.SetActive(true);
            savesScrollBar.SetActive(true);
            noSavesText.SetActive(false);

        }
        else
        {
            loadAndDeleteButtons.SetActive(false);
            savesScrollBar.SetActive(false);
            noSavesText.SetActive(true);
        }

        loadGameMenu.SetActive(true);
    }

    public void StartNewGame() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    public void ContinueGame() => SaveManager.instance.ContinueGame();

    public void LoadGame() => SaveManager.instance.LoadGame();

    public void DeleteSave()
    {
        SaveManager.instance.DeleteSave();
        ShowLoadMenu();
    }

    public void ChangeLanguage(string fileName)
    {
        FindObjectOfType<LocalizationManager>().LoadLocalizedText(fileName);
        Destroy(FindObjectOfType<PauseMenu>());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame() => Application.Quit();

}
