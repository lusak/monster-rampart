﻿using UnityEngine;
using System.Collections;


public class Wave : MonoBehaviour
{
    public MonsterSpawner[] monsterSpawners;

    public void StartWave()
    {
        foreach (MonsterSpawner spawner in monsterSpawners)
        {
            spawner.StartCoroutine(spawner.StartSpawning());
        }
    }
}
