﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelOne_Intro : MonoBehaviour
{
    private string[] LEVEL_ONE_INTRO_INFO = new string[] {
        "level_1_intro_text_0",
        "level_1_intro_text_1",
        "level_1_intro_text_2",
        "level_1_intro_text_3",
        "level_1_intro_text_4",
        "level_1_intro_text_5",
        "level_1_intro_text_6",
        "level_1_intro_text_7",
        "level_1_intro_text_8",
        "level_1_intro_text_9"}; 

    [SerializeField] private GameObject scoreArrow;
    [SerializeField] private GameObject timerArrow;
    [SerializeField] private GameObject wallArrow;
    [SerializeField] private GameObject canonArrow;
    [SerializeField] private GameObject castleArrow;
    [SerializeField] private LocalizedText text;

    [SerializeField] private GameObject buttonsPanel;
    [SerializeField] private GameObject buttonsPanelMock;

    private float tutorialSectionTime = 5f;

    void Start() => StartCoroutine(StartTutorial());

    private IEnumerator StartTutorial()
    {
        text.UpdateText(LEVEL_ONE_INTRO_INFO[0]);
        yield return new WaitForSeconds(tutorialSectionTime);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[1]);
        castleArrow.SetActive(true);
        yield return new WaitForSeconds(tutorialSectionTime);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[2]);
        yield return new WaitForSeconds(tutorialSectionTime);
        castleArrow.SetActive(false);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[3]);
        wallArrow.SetActive(true);
        yield return new WaitForSeconds(tutorialSectionTime);
        wallArrow.SetActive(false);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[4]);
        canonArrow.SetActive(true);
        yield return new WaitForSeconds(tutorialSectionTime);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[5]);
        yield return new WaitForSeconds(tutorialSectionTime);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[6]);
        yield return new WaitForSeconds(tutorialSectionTime);
        canonArrow.SetActive(false);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[7]);
        scoreArrow.SetActive(true);
        yield return new WaitForSeconds(tutorialSectionTime);
        scoreArrow.SetActive(false);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[8]);
        timerArrow.SetActive(true);
        yield return new WaitForSeconds(tutorialSectionTime);
        timerArrow.SetActive(false);
        text.UpdateText(LEVEL_ONE_INTRO_INFO[9]);
        yield return new WaitForSeconds(tutorialSectionTime * 2);
        text.gameObject.SetActive(false);

        FinishTutorial();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            StopAllCoroutines();
            FinishTutorial();
        }
    }

    private void FinishTutorial() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
}
