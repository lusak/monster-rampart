﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreText : MonoBehaviour
{
    private static int scorePoints;
    private static TextMeshProUGUI text;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();

    private void Start()
    {
        text.text = scorePoints.ToString();
        text.text = scorePoints.ToString();
    }

    public static int ScorePoints
    {
        get
        {
            return scorePoints;
        }

        set
        {
            scorePoints = value;
            text.text = scorePoints.ToString();           
        }
    }
}
