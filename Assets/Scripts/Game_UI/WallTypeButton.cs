﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class WallTypeButton : MonoBehaviour
{
    [SerializeField] private Block blockPrefab;
    [SerializeField] private bool selectedAtStart = false;

    private LevelPhaseManager buildingPhaseManager;
    private Color notSelectedColor;
    private InfoDisplay infoDisplay;
    private Image image;
    private WallButtonsManager wallButtonManager;
    private WallSpawner blockSpawner;
    private string wallInfo;

    private void Awake()
    {
        infoDisplay = FindObjectOfType<InfoDisplay>();
        image = GetComponent<Image>();
        buildingPhaseManager = FindObjectOfType<LevelPhaseManager>();
        wallButtonManager = FindObjectOfType<WallButtonsManager>();
        blockSpawner = FindObjectOfType<WallSpawner>();
        notSelectedColor = image.color;
        wallInfo = BuildInfoString();
    }

    private void Start()
    {
        if (selectedAtStart)
        {
            Select();
            infoDisplay.SetText(wallInfo);
        }
    }

    private void OnMouseDown() => SelectButton();

    public void SelectButton()
    {
        wallButtonManager.UnselectAllButtons();
        blockSpawner.blockPrefab = blockPrefab;
        blockSpawner.ChangeCurrentShapeMaterial();
        buildingPhaseManager.StartWallPlacing();
        image.color = Color.white;
        infoDisplay.SetText(wallInfo);
    }

    public void Unselect() => image.color = notSelectedColor;

    public void Select() => image.color = Color.white;

    private string BuildInfoString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(blockPrefab.Cost.ToString() + "$");
        sb.Append("   ");
        sb.Append(blockPrefab.GetComponent<Health>().HitPoints + "HP");
        return sb.ToString();
    }
}
