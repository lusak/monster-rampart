﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    private TextMeshProUGUI text;
    private int time;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();


    public void SetTimer(int seconds)
    {
        gameObject.SetActive(true);
        StartCoroutine(StartTimer(seconds));
    }

    public void HideTimer() => gameObject.SetActive(false);


    private IEnumerator StartTimer(int seconds)
    {
        for (int i = seconds; i >= 0; i--)
        {
            time = i;
            text.text = time.ToString();
            yield return new WaitForSeconds(1f);
        }
        gameObject.SetActive(false);
    }
}
