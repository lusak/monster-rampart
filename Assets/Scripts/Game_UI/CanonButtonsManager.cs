﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonButtonsManager : MonoBehaviour
{
    private CanonTypeButton[] buttons;

    void Awake() => buttons = FindObjectsOfType<CanonTypeButton>();

    public void UnselectAllButtons()
    {
        foreach (CanonTypeButton button in buttons)
        {
            button.Unselect();
        }
    }
}
