﻿using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class CanonTypeButton : MonoBehaviour
{
    [SerializeField] private Canon canonPrefab;

    private LevelPhaseManager buildingPhaseManager;
    private CanonButtonsManager canonButtonsManager;
    private InfoDisplay infoDisplay; 
    private CanonSpawner canonSpawner;
    private Color notSelectedColor;
    private Image image;
    private string canonInfo;

    private void Awake()
    {
        image = GetComponent<Image>();
        infoDisplay = FindObjectOfType<InfoDisplay>();
        canonSpawner = FindObjectOfType<CanonSpawner>();
        canonButtonsManager = FindObjectOfType<CanonButtonsManager>();
        buildingPhaseManager = FindObjectOfType<LevelPhaseManager>();
        notSelectedColor = image.color;
    }

    private void Start() => canonInfo = BuildInfoString();

    private void OnMouseDown() => SelectButton();

    public void Unselect() => image.color = notSelectedColor;

    public void Select() => image.color = Color.white;

    public void SelectButton()
    {
        canonButtonsManager.UnselectAllButtons();
        canonSpawner.ChangeCanonPrefab(canonPrefab);
        buildingPhaseManager.StartCanonPlacing();
        image.color = Color.white;
        infoDisplay.SetText(canonInfo);
    }

    private string BuildInfoString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(canonPrefab.Cost.ToString() + "$");
        sb.Append("   ");
        sb.Append(canonPrefab.GetDamagePerSecond() + " DMG/s");
        sb.Append("   ");
        sb.Append(canonPrefab.GetComponent<Health>().HitPoints + "HP");

        return sb.ToString();
    }
}
