﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallButtonsManager : MonoBehaviour
{
    [SerializeField] private WallTypeButton defaultWallTypeButton;

    private WallTypeButton[] buttons;

    private void Awake() => buttons = FindObjectsOfType<WallTypeButton>();

    public void UnselectAllButtons()
    {
        foreach (WallTypeButton button in buttons)
        {
            button.Unselect();
        }
    }

    public void SelectDefaultWallButton() => defaultWallTypeButton.SelectButton();
}
