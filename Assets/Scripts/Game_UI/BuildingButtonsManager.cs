﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingButtonsManager : MonoBehaviour
{

    [SerializeField] private WallTypeButton woodenWallButton;
    [SerializeField] private WallTypeButton stoneWallButton;
    [SerializeField] private WallTypeButton brickWallButton;

    [SerializeField] private CanonTypeButton simpleCanonButton;
    [SerializeField] private CanonTypeButton doubleCanonButton;
    [SerializeField] private CanonTypeButton rocketCanonButton;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            if(woodenWallButton)
            {
                woodenWallButton.SelectButton();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (stoneWallButton)
            {
                stoneWallButton.SelectButton();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (brickWallButton)
            {
                brickWallButton.SelectButton();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (simpleCanonButton)
            {
                simpleCanonButton.SelectButton();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (doubleCanonButton)
            {
                doubleCanonButton.SelectButton();
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            if (rocketCanonButton)
            {
                rocketCanonButton.SelectButton();
            }
        }
    }
}
