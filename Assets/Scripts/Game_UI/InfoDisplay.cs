﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InfoDisplay : MonoBehaviour
{
    private TextMeshProUGUI textField;

    private void Start() => textField = GetComponent<TextMeshProUGUI>();


    public void SetText(string text)
    {
        if (!textField)
        {
            textField = GetComponent<TextMeshProUGUI>();
        }
        textField.text = text;
    }
}
