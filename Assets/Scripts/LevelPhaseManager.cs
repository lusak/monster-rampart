﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class LevelPhaseManager : MonoBehaviour
{
    [SerializeField] private int buildingPhaseDuration = 15;
    [SerializeField] private int startingCash = 500;
    [SerializeField] private GameObject buttonsPanel;
    [SerializeField] private GameObject levelCompleteText;
    [SerializeField] private GameObject levelFailedText;

    public Wave[] waves;

    private Timer timer;
    private WallSpawner wallSpawner;
    private CanonSpawner canonSpawner;
    private WallButtonsManager wallButtonsManager;
    private CanonButtonsManager canonButtonsManager;
    private Canon[] canons;
    private int currentWaveIndex = 0;
    private int currentWaveActiveSpawners;
    private bool waveFinished = false;
    private bool levelFinished = false;

    private bool isBuildingPhase = false;

    private void Awake()
    {
        timer = FindObjectOfType<Timer>();
        wallSpawner = FindObjectOfType<WallSpawner>();
        canonSpawner = FindObjectOfType<CanonSpawner>();
        wallButtonsManager = FindObjectOfType<WallButtonsManager>();
        canonButtonsManager = FindObjectOfType<CanonButtonsManager>();
    }

    private void Start()
    {
        ScoreText.ScorePoints = startingCash;
        StartLevel();
    }

    private void StartLevel()
    {
        Monster.ResetCastleArray();
        PauseMenu.instance.GameOver = false;
        currentWaveActiveSpawners = waves[currentWaveIndex].monsterSpawners.Length;
        wallSpawner.InitCurrentShape();
        wallSpawner.Active = true;
        StartCoroutine(StartNewWave());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace) && isBuildingPhase)
        {
            SkipBuildPhase();
        }
        if (waveFinished)
        {
            if (!FindObjectOfType<Monster>())
            {
                FinishWave();
            }
        }
    }

    public void HandleCastleDestroyed()
    {
        if (!levelFinished)
        {
            StopAllCoroutines();
            FinishBattle();
            StartCoroutine(LevelFailed());
        }
    }

    public void StartWallPlacing()
    {
        canonSpawner.Active = false;
        canonButtonsManager.UnselectAllButtons();
        wallSpawner.Active = true;
    }

    public void StartCanonPlacing()
    {
        wallSpawner.Active = false;
        wallButtonsManager.UnselectAllButtons();
        canonSpawner.Active = true;
    }

    public void RegisterSpawnerFinishedSpawning()
    {
        if (--currentWaveActiveSpawners <= 0)
        {
            waveFinished = true;
        }
    }

    private void StartBattle() 
    {
        wallSpawner.Active = false;
        canonSpawner.Active = false;
        canons = FindObjectsOfType<Canon>();
        foreach (Canon canon in canons)
        {
            canon.Active = true;
        }
    }

    private void FinishBattle()
    {
        canons = FindObjectsOfType<Canon>();
        foreach (Canon canon in canons)
        {
            canon.Active = false;
        }
    }

    private void FinishWave()
    {
        waveFinished = false;
        FinishBattle();
        if (currentWaveIndex == waves.Length - 1)
        {
            levelFinished = true;
            StartCoroutine(LevelComplete());

        } else
        {
            currentWaveIndex++;
            StartCoroutine(StartNewWave());           
        }
    }

    private void SkipBuildPhase()
    {
        isBuildingPhase = false;
        StopAllCoroutines();
        timer.HideTimer();
        HideButtonsPanel();
        StartBattle();
        waves[currentWaveIndex].StartWave();
        currentWaveActiveSpawners = waves[currentWaveIndex].monsterSpawners.Length;
    }

    private IEnumerator StartNewWave()
    {
        isBuildingPhase = true;
        ShowButtonsPanel();
        timer.SetTimer(buildingPhaseDuration);
        StartWallPlacing();
        wallButtonsManager.SelectDefaultWallButton();
        yield return new WaitForSeconds(buildingPhaseDuration);
        HideButtonsPanel();
        isBuildingPhase = false;
        StartBattle();
        waves[currentWaveIndex].StartWave();
        currentWaveActiveSpawners = waves[currentWaveIndex].monsterSpawners.Length;
    }

    private IEnumerator LevelComplete()
    {
        levelCompleteText.SetActive(true);
        GameScore.Score += ScoreText.ScorePoints;
        Monster.ResetCastleArray();
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private IEnumerator LevelFailed()
    {
        levelFailedText.SetActive(true);
        yield return new WaitForSeconds(2);
        levelFailedText.SetActive(false);
        PauseMenu.instance.ShowGameOverMenu();
    }

    private void HideButtonsPanel() => buttonsPanel.SetActive(false);

    private void ShowButtonsPanel() => buttonsPanel.SetActive(true);
}
