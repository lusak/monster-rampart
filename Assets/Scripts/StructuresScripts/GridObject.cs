﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject : MonoBehaviour
{
    private Health health;
    private FreePlaceManager placeManager;

    void Awake()
    {
        health = GetComponent<Health>();
        placeManager = FindObjectOfType<FreePlaceManager>();
        health.onDeathEvent += OnDeath;
    }

    private void OnDestroy()
    {
        health.onDeathEvent -= OnDeath;
    }

    public void OnDeath()
    {
        placeManager.RemoveObjectFromGrid(transform);
        Destroy(gameObject);
    }


}
