﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private AudioClip fireClip;
    [SerializeField] private AudioClip explodeClip;
    [SerializeField] private int damage;
    [SerializeField] private bool isExplosive;

    private float explosionRadious = 3f;
    
    private Rigidbody2D rb;

    private void Awake() => rb = GetComponent<Rigidbody2D>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Monster monster = collision.gameObject.GetComponent<Monster>();
        if (monster)
        {
            monster.GetComponent<Health>().TakeDamage(damage);
            if (isExplosive)
            {
                StartCoroutine(Explode());
            } else
            {
                Destroy(gameObject);
            }
        }
    }

    public void Fire()
    {
        SoundManager.instance.PlayAudio(fireClip, 0.7f);
        rb.velocity = transform.up * speed;
    }

    public IEnumerator Explode()
    {
        rb.velocity = Vector2.zero;
        LayerMask mask = LayerMask.GetMask("Monsters");
        Collider2D[] monsters = Physics2D.OverlapCircleAll(transform.position, explosionRadious, mask);
        foreach (Collider2D monster in monsters)
        {
            monster.gameObject.GetComponent<Health>().TakeDamage(Damage);
        }
        GetComponent<Animator>().SetTrigger("explode");
        SoundManager.instance.PlayAudio(explodeClip, 1f);
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }

    public int Damage
    {
        get
        {
            return damage;
        }
    }

    public bool IsExplosive
    {
        get
        {
            return isExplosive;
        }
    }
}
