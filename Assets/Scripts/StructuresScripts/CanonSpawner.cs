﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonSpawner : MonoBehaviour
{
    public Canon canonPrefab;
    //Gameobject for keeping all canon objects
    public GameObject canonsGameobject;

    private FreePlaceManager freePlaceManager;
    private ScoreText score;
    private Vector2 mousePos;
    private bool active = false;
    private Canon currentCanon;
    private Camera camera;


    private void Awake()
    {
        freePlaceManager = FindObjectOfType<FreePlaceManager>();
        score = FindObjectOfType<ScoreText>();
        camera = Camera.main;
    }
    private void Start() => currentCanon = SpawnCanon();

    void Update()
    {
        if (active)
        {
            mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
            currentCanon.transform.position = new Vector2(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y));

            if (Input.GetMouseButtonDown(0))
            {
                if (!CheckIfEnoughScorePoints())
                {
                    //Not enough cash sound
                    return;
                }

                if(freePlaceManager.CheckIfPlaceFree(currentCanon.transform))
                {
                    freePlaceManager.AddObjectToGrid(currentCanon.transform);
                    currentCanon.gameObject.transform.parent = canonsGameobject.transform;
                    ScoreText.ScorePoints -= canonPrefab.Cost;
                    currentCanon = SpawnCanon();
                }
            }
        }
        
    }

    public Canon SpawnCanon() => GameObject.Instantiate(canonPrefab, new Vector3(mousePos.x, mousePos.y, 0f), Quaternion.identity);

    public void ChangeCanonPrefab(Canon newCanonPrefab)
    {
        canonPrefab = newCanonPrefab;
        Destroy(currentCanon.gameObject);
        currentCanon = SpawnCanon();
    }

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
            if (value)
            {
                currentCanon?.gameObject.SetActive(true);
            }
            else
            {
                currentCanon?.gameObject.SetActive(false);
            }
        }
    }

    private bool CheckIfEnoughScorePoints()
    {
        if (ScoreText.ScorePoints >= canonPrefab.Cost)
        {
            return true;
        }

        return false;
    }
}
