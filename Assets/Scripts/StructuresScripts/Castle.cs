﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : MonoBehaviour
{
    private LevelPhaseManager phaseManager;
    private Health health;

    private void Awake()
    {
        health = GetComponent<Health>();
        health.onDeathEvent += OnDeath;
        phaseManager = FindObjectOfType<LevelPhaseManager>();
    }

    void OnDestroy()
    {
        health.onDeathEvent += OnDeath;
    }

    public void OnDeath()
    {
        if (phaseManager)
        {
            phaseManager.HandleCastleDestroyed();
        }
        Destroy(gameObject);
    }
}
