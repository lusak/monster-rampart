﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour
{
    [SerializeField] private int cost;
    [SerializeField] private float reloadTime;
    [SerializeField] private Projectile projectilePrefab;

    public bool Active { get; set; }

    private SpriteRenderer spriteRenderer;
    private float lastTimeFired;
    private Vector3 mousePos;
    private Camera camera;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        camera = Camera.main;
    }

    private void Update()
    {
        if(Active)
        {
            RotateTowardsMouse();

            if (Input.GetMouseButton(0))
            {
                if (Time.time - lastTimeFired >= reloadTime)
                {
                    Fire();
                }
            }
        }
    }

    public void Fire()
    {
        Projectile projectile = Instantiate(projectilePrefab, transform.position, transform.rotation);
        projectile.Fire();
        lastTimeFired = Time.time;
    }

    public float GetDamagePerSecond() => projectilePrefab.Damage / reloadTime;

    public void RemoveSpriteTransparency()
    {
        Color oldColor = spriteRenderer.color;
        spriteRenderer.color = new Color(oldColor.r, oldColor.g, oldColor.b, 1);
    }


    public int Cost
    {
        get
        {
            return cost;
        }

        set
        {
            cost = value;
        }
    }

    private void RotateTowardsMouse()
    {
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
        transform.up = direction;
    }
}
