﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class WallSpawner : MonoBehaviour
{
    public Block blockPrefab;
    public ShapeInfo[] shapeInfos;
    //Gameobject for keeping all wall objects
    public GameObject blocksGameobject;

    private FreePlaceManager freePlaceManager;
    private ScoreText score;
    private ShapeInfo currenShapeInfo;
    private GameObject currentShape;
    private Vector2 mousePos;
    private Camera camera;

    private bool active = false;

    private void Awake()
    {
        score = FindObjectOfType<ScoreText>();
        freePlaceManager = FindObjectOfType<FreePlaceManager>();
        camera = Camera.main;
    }

    void Update()
    {
        if (active)
        {
            mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
            currentShape.transform.position = new Vector2(Mathf.Round(mousePos.x), Mathf.Round(mousePos.y));

            if (Input.GetKeyDown(KeyCode.Space))
            {
                currentShape.transform.Rotate(0f, 0f, 90f);
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (!CheckIfEnoughScorePoints())
                {
                    //Play Sound not enough cash
                    return;
                }

                bool canBePlaced = freePlaceManager.AddMultipleObjectsToGrid(currentShape.transform);

                if (canBePlaced)
                {
                    foreach (Transform obj in currentShape.transform)
                    {
                        ScoreText.ScorePoints -= blockPrefab.Cost;
                    }

                    RemoveShapeTransparency();
                    currentShape = SpawnShape();
                }
            }
        }
    }

    public void InitCurrentShape() => currentShape = SpawnShape();

    public void ChangeCurrentShapeMaterial()
    {
        Destroy(currentShape);
        GameObject newShape = new GameObject("Block " + Time.time);
        newShape.transform.parent = blocksGameobject.transform;
        foreach (Vector2 position in currenShapeInfo.positions)
        {
            Block newBlock = GameObject.Instantiate(blockPrefab, new Vector3(position.x, position.y, 0f), Quaternion.identity);
            newBlock.transform.parent = newShape.transform;
        }
        currentShape = newShape;
    }

    public bool Active
    {
        get
        {
            return active;
        }

        set
        {
            active = value;
            if (value)
            {
                currentShape?.SetActive(true);
            } else {
                currentShape?.SetActive(false);
            }
        }
    }

    private void RemoveShapeTransparency()
    {
        foreach (Transform trans in currentShape.transform)
        {
            trans.gameObject.GetComponent<Block>().RemoveSpriteTransparency();
        }
    }

    private GameObject SpawnShape()
    {
        GameObject newShape = new GameObject("Block " + Time.time);
        newShape.transform.parent = blocksGameobject.transform;
        ShapeInfo shapeInfo = shapeInfos[UnityEngine.Random.Range(0, shapeInfos.Length)];
        currenShapeInfo = shapeInfo;
        foreach (Vector2 position in shapeInfo.positions)
        {
            Block newBlock = GameObject.Instantiate(blockPrefab, new Vector3(position.x, position.y, 0f), Quaternion.identity);
            newBlock.transform.parent = newShape.transform;
        }
        return newShape;
    }

    private bool CheckIfEnoughScorePoints()
    {
        int cost = currentShape.transform.childCount * blockPrefab.Cost;

        if (ScoreText.ScorePoints >= cost)
        {
            return true;
        }

        return false;
    }
}