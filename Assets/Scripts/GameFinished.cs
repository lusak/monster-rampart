﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameFinished : MonoBehaviour
{
    public TextMeshProUGUI scoreText;

    private void Start()
    {
        HighscoreSystem.AddHighscore(GameScore.Score);
        scoreText.text = GameScore.Score.ToString();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            SceneManager.LoadScene(1);
        }
    }
}
