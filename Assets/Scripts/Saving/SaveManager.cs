﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;

    private SaveData activeSave;
    private string activeSavePath;
    public string SelectedSavePath { get; set; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }


    public void ContinueGame()
    {
        ChangeActiveDataWithPlayerPrefs();
        if (activeSave != null)
        {
            GameScore.Score = activeSave.score;
            SceneManager.LoadScene(activeSave.sceneIndex);
        }
    }

    public void LoadGame()
    {
        if (!string.IsNullOrEmpty(SelectedSavePath))
        {
            activeSave = SaveSystem.LoadSave(SelectedSavePath);
            activeSavePath = SelectedSavePath;
            PlayerPrefs.SetString(SaveSystem.PLAYER_PREFS_LAST_SAVE_PATH, SelectedSavePath);
            GameScore.Score = activeSave.score;
            SceneManager.LoadScene(activeSave.sceneIndex);
        }
    }

    public void SaveGame(string fileName)
    {
        SaveSystem.SaveGame(SceneManager.GetActiveScene().buildIndex, GameScore.Score, fileName);
        ChangeActiveDataWithPlayerPrefs();
    }

    public void DeleteSave()
    {
        if (!string.IsNullOrEmpty(SelectedSavePath))
        {
            SaveSystem.DeleteSave(SelectedSavePath);
            SelectedSavePath = null;
        }
    }

    public void OverwriteLastSave()
    {
        if (activeSave != null && !string.IsNullOrEmpty(activeSavePath))
        {
            SaveSystem.UpdateSave(activeSave, activeSavePath);
        }
    }

    public void ChangeActiveDataWithPlayerPrefs()
    {
        activeSavePath = PlayerPrefs.GetString(SaveSystem.PLAYER_PREFS_LAST_SAVE_PATH);
        activeSave = SaveSystem.LoadSave(PlayerPrefs.GetString(SaveSystem.PLAYER_PREFS_LAST_SAVE_PATH));
    }
}
