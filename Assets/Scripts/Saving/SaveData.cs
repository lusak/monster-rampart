﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class SaveData
{
    public int sceneIndex;
    public int score;

    public SaveData(int sceneIndex, int score)
    {
        this.sceneIndex = sceneIndex;
        this.score = score;
    }
}
