﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public static class SaveSystem
{
    public const string PLAYER_PREFS_LAST_SAVE_PATH = "lastSavePath";
    private static SaveData activeData;
    private static SaveData selectedData;

    public static string[] GetSavesPaths() => Directory.GetFiles(Application.persistentDataPath, "*.mr");

    public static string[] GetSavesFileNames()
    {
        string[] filePaths = Directory.GetFiles(Application.persistentDataPath, "*.mr");
        List<string> fileNames = new List<string>();

        foreach (string filepath in filePaths)
        {
            string fileName = Path.GetFileNameWithoutExtension(filepath);
            fileNames.Add(fileName);
        }
        return fileNames.ToArray();
    }

    public static SaveData SaveGame(int sceneIndex, int score, string fileName)
    {
        Debug.Log("Saving file");
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/" + fileName + ".mr"; 
        FileStream stream = new FileStream(path, FileMode.Create);

        SaveData data = new SaveData(sceneIndex, score);

        formatter.Serialize(stream, data);
        stream.Close();
        PlayerPrefs.SetString(PLAYER_PREFS_LAST_SAVE_PATH, path);
        return data;
    }

    public static void UpdateSave(SaveData saveData, string path)
    {
        Debug.Log("Updating file");
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);
        formatter.Serialize(stream, saveData);
        stream.Close();
    }

    public static SaveData LoadSave(string path)
    {
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();

            return data;

        } else
        {
            Debug.LogError("Save file not found " + path);
            return null;
        }
    }

    public static void DeleteSave(string path)
    {
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Debug.LogError("Save file not found " + path);
        }
    }

    public static bool CheckSaveFileExists(string fileName)
    {
        string[] saveFiles = GetSavesFileNames();
        foreach (string save in saveFiles)
        {
            if (save == fileName)
            {
                return true;
            }
        }
        return false;
    }

}
