﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class SaveButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    private string savePath;

    public void SetSaveButton(string saveFilePath)
    {
        savePath = saveFilePath;

        saveFilePath = Path.GetFileName(saveFilePath);
        if (saveFilePath.Length > 25)
        {
            saveFilePath = saveFilePath.Substring(0, 25);
        }
        text.text = saveFilePath;
    }

    public void OnClick() => FindObjectOfType<SaveManager>().SelectedSavePath = savePath;


}
