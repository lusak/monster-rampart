﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LocalizationManager : MonoBehaviour
{
    private const string MISSING_TEXT_ERROR = "Localized text not found";
    private const string ENGLISH_LOCALIZED_TEXT_FILE = "localizedText_en.json";

    private const string USER_PREFFERED_LOCALIZATION_FILE_FIELD = "preffered_localization_file";

    public static LocalizationManager instance;

    private Dictionary<string, string> localizedText;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            if (!LoadLocalizedText(PlayerPrefs.GetString(USER_PREFFERED_LOCALIZATION_FILE_FIELD)))
            {
                LoadLocalizedText(ENGLISH_LOCALIZED_TEXT_FILE);
            }
        } else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public bool LoadLocalizedText(string fileName)
    {
        localizedText = new Dictionary<string, string>();
        string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
        PlayerPrefs.SetString(USER_PREFFERED_LOCALIZATION_FILE_FIELD, fileName);

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

            for (int i = 0; i < loadedData.items.Length; i++)
            {
                localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
            }
            PlayerPrefs.SetString(USER_PREFFERED_LOCALIZATION_FILE_FIELD, fileName);
            Debug.Log("Data loaded. Text dictionary contains " + localizedText.Count + " entries");
            return true;
        }
        else
        {
            Debug.LogError("Cannot find localization file!");
            return false;
        }       
    }

    public string GetLocalizedValue(string key)
    {
        string result = MISSING_TEXT_ERROR;
        if(localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;
    }
}
