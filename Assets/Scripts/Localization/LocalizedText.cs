﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string key;
    private TextMeshProUGUI textComponent;

    void Awake() => textComponent = GetComponent<TextMeshProUGUI>();


    private void Start() => textComponent.text = LocalizationManager.instance.GetLocalizedValue(key);

    public void UpdateText(string key) => textComponent.text = LocalizationManager.instance.GetLocalizedValue(key);
}
