﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public MonsterBatch[] batches;

    private LevelPhaseManager phaseManager;

    private void Awake() => phaseManager = FindObjectOfType<LevelPhaseManager>();

    public IEnumerator StartSpawning()
    {
        foreach (MonsterBatch batch in batches)
        {
            yield return StartCoroutine(SpawnMonsterBatch(batch));
            yield return new WaitForSeconds(batch.timeAfterBatchEnds);
        }

        phaseManager.RegisterSpawnerFinishedSpawning();
    }

    private IEnumerator SpawnMonsterBatch(MonsterBatch batch)
    {
        for (int i = 0; i < batch.batchSize; i++)
        {
            Instantiate(batch.monster, transform.position, transform.rotation);
            yield return new WaitForSeconds(batch.timeBetweenSpawns);
        }
    }
}
