﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class MonsterBatch : ScriptableObject
{
    public Monster monster;
    public int batchSize;
    public int timeBetweenSpawns;
    public int timeAfterBatchEnds;
}
