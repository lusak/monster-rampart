﻿using UnityEngine;
using System.Collections;
using System.Dynamic;

public class Monster : MonoBehaviour
{
    private static Castle[] castlesInTheScene;

    [SerializeField] private int damage;
    [SerializeField] private float speed = 2f;
    [SerializeField] private int scorePoints = 20;
    [SerializeField] private AudioClip deathClip;

    private Castle castleToAttack;
    private Animator animator;
    private bool moving = true;
    private Health health;
    private Health currentTarget;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        health = GetComponent<Health>();

        if (castlesInTheScene == null)
        {
            castlesInTheScene = FindObjectsOfType<Castle>();
        }

        health.onDeathEvent += OnDeath;
        castleToAttack = castlesInTheScene[Random.Range(0, castlesInTheScene.Length)];
    }

    void Update()
    {
        if (moving)
        {
            Move();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.GetComponent<Monster>())
        {
            return;
        }

        if (collision.gameObject.GetComponent<Health>())
        {
            currentTarget = collision.gameObject.GetComponent<Health>();
            moving = false;
            StartCoroutine(AttackCoroutine());
        }
    }

    private void OnDestroy()
    {
        health.onDeathEvent -= OnDeath;
    }

    public void OnDeath()
    {
        ScoreText.ScorePoints += scorePoints;
        SoundManager.instance.PlayAudio(deathClip, 0.9f);
        Destroy(gameObject);
    }


    public static void ResetCastleArray() => castlesInTheScene = null;

    private void Move()
    {
        if (castleToAttack)
        {
            float x = castleToAttack.transform.position.x - transform.position.x;
            float y = castleToAttack.transform.position.y - transform.position.y;

            animator.SetFloat("moveX", x);
            animator.SetFloat("moveY", y);

            transform.position = Vector3.MoveTowards(transform.position, castleToAttack.transform.position, speed * Time.deltaTime);
        }
    }

    private IEnumerator AttackCoroutine()
    {
        while (currentTarget)
        {
            currentTarget.TakeDamage(damage);
            yield return new WaitForSeconds(1f);
        }
        moving = true;
    }

    public int Damage
    {
        get
        {
            return damage;
        }
    }
}
