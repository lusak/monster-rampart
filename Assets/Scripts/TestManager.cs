﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestManager : MonoBehaviour
{
    //Class just for speeding up testing
    public void LoadScene(int scenIndex)
    {
        SceneManager.LoadScene(scenIndex);
    }
}
