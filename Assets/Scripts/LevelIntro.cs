﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelIntro : MonoBehaviour
{
    private float introSectionWaitTime = 15f;

    void Start() => StartCoroutine(StartIntro());

    private IEnumerator StartIntro()
    {
        yield return new WaitForSeconds(introSectionWaitTime);

        FinishIntro();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            StopAllCoroutines();
            FinishIntro();
        }
    }

    private void FinishIntro() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
}
