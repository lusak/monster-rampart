﻿using UnityEngine;
using System.Collections;
using System;

public class Health : MonoBehaviour
{
    [SerializeField] private int hitPoints = 100;

    public event Action onDeathEvent;

    public void TakeDamage(int damage)
    {
        hitPoints -= damage;

        if (hitPoints <= 0)
        {
            OnDeath();
        }
    }

    public void OnDeath()
    {
        onDeathEvent?.Invoke();
    }

    public int HitPoints
    {
        get
        {
            return hitPoints;
        }
        set
        {
            hitPoints = value;
        }
    }

}
