﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreePlaceManager : MonoBehaviour
{
    [SerializeField] private GameObject initialWalls;

    //Numbers are unnecessarily too big. Think what to do with it
    private static int width = 100;
    private static int height = 100;
    private bool[,] defensiveObjectsGrid = new bool[width, height];


    private void Start() => AddMultipleObjectsToGrid(initialWalls.transform);


    public bool AddMultipleObjectsToGrid(Transform objects)
    {
        bool canBeAdded = false;
        foreach (Transform obj in objects)
        {
            canBeAdded = CheckIfPlaceFree(obj);
            if(!canBeAdded)
            {
                Debug.Log("Place not free");
                return false;
            }
        }

        foreach (Transform obj in objects)
        {
            AddObjectToGrid(obj);
        }

        return true;
    }

    public bool CheckIfPlaceFree(Transform obj)
    {
        Vector2 objPos = obj.position;
        RaycastHit2D[] hits = Physics2D.RaycastAll(objPos, Vector2.zero);
        bool isOnWallsArea = false;
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.collider.gameObject.tag == "BuildingArea")
            {
                isOnWallsArea = true;
            }
        }

        if (!isOnWallsArea)
        {
            return false;
        }

        int xPos = (int)obj.position.x;
        int yPos = (int)obj.position.y;

        if (defensiveObjectsGrid[xPos, yPos] == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void AddObjectToGrid(Transform obj)
    {
        int xPos = (int)obj.position.x;
        int yPos = (int)obj.position.y;
        defensiveObjectsGrid[xPos, yPos] = true;
    }

    public void RemoveObjectFromGrid(Transform obj)
    {
        int xPos = (int)obj.position.x;
        int yPos = (int)obj.position.y;
        defensiveObjectsGrid[xPos, yPos] = false;
    }
}
